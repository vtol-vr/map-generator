import { MersenneTwister } from 'random-seedable';
import pointInPolygon from 'point-in-polygon';
import { CoastSide, EdgeMode, MapTypeGeneratorOutput, Settings, VoronoiCell } from '../types';

export function generateSingleIsland(settings: Settings, random: MersenneTwister, voronoiCells: VoronoiCell[]): MapTypeGeneratorOutput {
  const waterBorder = 0.2;
  const islandCenterX = (random.float() * settings.worldWidth * (1 - (2 * waterBorder))) + (settings.worldWidth * waterBorder);
  const islandCenterY = (random.float() * settings.worldWidth * (1 - (2 * waterBorder))) + (settings.worldWidth * waterBorder);
  let firstCell: VoronoiCell = voronoiCells[0];
  //Set all to deep sea and find island center cell
  for(let voronoiCell of voronoiCells) {
    voronoiCell.initialHeight = -80;
    if(pointInPolygon([islandCenterX, islandCenterY], voronoiCell.points)) {
      firstCell = voronoiCell;
    }
  }
  //Create fault lines
  let faultLineCells: VoronoiCell[] = [];
  const faultLineNumber = Math.floor(random.randRange(2, 5));
  for(let j = 0;j < faultLineNumber;j++) {
    let faultLineLength = Math.floor(random.randRange(settings.chunks, settings.chunks * 3));
    let faultLineHeight = random.randRange(5000, 10000);
    let randomDirection = random.float() * 2 * Math.PI;
    let currCell = firstCell;
    for(let i = 0;i < faultLineLength;i++) {
      currCell.initialHeight = faultLineHeight;
      faultLineCells.push(currCell);
      //Find angles for all neighbors
      let neighborAngles = currCell.neighbors.map((n) => {
        let neighbor = voronoiCells[n];
        return Math.atan2(neighbor.center[1] - currCell.center[1], neighbor.center[0] - currCell.center[0]);
      });
      //Find difference between neighbor angle and random direction
      let angleDifferences = neighborAngles.map((a) => {
        let diff = Math.abs(randomDirection - a);
        if(diff > Math.PI) {
          return Math.abs(diff - Math.PI);
        }
        return diff;
      });
      //Inverted percentage (0 is opposite direction, 1 is same)
      let invertedPercentages = angleDifferences.map((d) => (Math.PI - d) / Math.PI);
      //Create weighted array
      let percentageSum = invertedPercentages.reduce((p,c) => p + c);
      let weightedArray = invertedPercentages.map(p => p / percentageSum);
      //Choose random neighbor based off of weight
      let currWeight = 0;
      let chosenIndex = -1;
      let threshold = random.float();
      for(let j = 0;j < weightedArray.length;j++) {
        currWeight += weightedArray[j];
        if(weightedArray[j] >= threshold && chosenIndex < 0) {
          chosenIndex = j;
        }
      }
      if(chosenIndex < 0) {
        chosenIndex = weightedArray.length - 1;
      }
      currCell = voronoiCells[currCell.neighbors[chosenIndex]];
    }
  }
  /*
  //Add Simplex noise
  const simplex = createNoise2D(() => { return random.float(); });
  for(let voronoiCell of voronoiCells) {
    voronoiCell.simplexNoiseHeight = (simplex(voronoiCell.center[0], voronoiCell.center[1]) * 1000) + voronoiCell.initialHeight;
  }
  */
  return {
    voronoiCells: voronoiCells,
    edgeMode: EdgeMode.Water,
    coastSide: CoastSide.North
  };
}