import Plotly from 'plotly.js-dist-min';
import { colorFromHeight } from './renderUtilities';
import { Biome, HeightMap } from './types';

export default class Chart {
  map3DElement: HTMLDivElement = document.getElementById('map3D') as HTMLDivElement;
  layout = {
    title: 'Height Map',
    autosize: true,
    scene:{
      aspectratio: {
        x: 1,
        y: 1,
        z: 1/15.36
      },
      zaxis: {
        range: [-80, 6000]
      }
    }
  };
  data(data: HeightMap) {
    const chartData = [{
      z: data,
      cmin: -81,
      cmax: 6000,
      colorscale: this.generateColorScale(),
      type: 'surface'
    }];
    Plotly.newPlot(this.map3DElement, chartData, this.layout, { responsive: true });
  }
  generateColorScale(): any[][] {
    let colors: any[][] = [];
    for(let i = -80;i <= 6000;i+=10) {
      let currColor = this.colorToString(colorFromHeight(i, Biome.Boreal));
      if(typeof colors[colors.length - 1] !== 'undefined' && colors[colors.length - 1][1] !== currColor) {
        colors.push([(i + 70)/6080, this.colorToString(colorFromHeight(i - 10, Biome.Boreal))]);
        colors.push([(i + 80)/6080, currColor]);
      }
      else if(colors.length <= 0 || i === 6000) {
        colors.push([(i + 80)/6080, currColor]);
      }
    }
    return colors;
  }
  colorToString(number): string {
    return '#' + number.toString(16).padStart(6, '0');
  }
}