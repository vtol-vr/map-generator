import { Biome, CoastSide, EdgeMode, HeightMap, MapType, Point, Settings, VoronoiCell } from './types';
import { Delaunay } from 'd3-delaunay';

export default class DataManager {
  settings: Settings = {
    mapName: 'Untitled Map',
    missionName: 'Untitled Mission',
    mapType: MapType.SingleIsland,
    biome: Biome.Boreal,
    edgeMode: EdgeMode.Water,
    coastSide: CoastSide.North,
    chunks: 8,
    worldWidth: (8 * 20) + 1,
    seed: Math.floor(Math.random() * 900000) + 100000,
    voronoiBlurPass: 5,
    gridBlurPass: 5,
    step: 0
  };
  poissonPoints: Point[];
  delaunay: Delaunay;
  voronoi: Delaunay.Voronoi;
  voronoiCells: VoronoiCell[];
  heightMap: HeightMap;
}