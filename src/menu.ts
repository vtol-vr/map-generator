import { Biome, CoastSide, EdgeMode, MapType, Settings } from './types';

export default class Menu {
  mapNameElement: HTMLInputElement = document.getElementById('mapName') as HTMLInputElement;
  missionNameElement: HTMLInputElement = document.getElementById('missionName') as HTMLInputElement;
  seedElement: HTMLInputElement = document.getElementById('seed') as HTMLInputElement;
  newSeedButtonElement: HTMLButtonElement = document.getElementById('newSeedButton') as HTMLButtonElement;
  chunksElement: HTMLInputElement = document.getElementById('chunks') as HTMLInputElement;
  mapTypeElement: HTMLSelectElement = document.getElementById('mapType') as HTMLSelectElement;
  biomeElement: HTMLSelectElement = document.getElementById('biome') as HTMLSelectElement;
  voronoiBlurPassElement: HTMLSelectElement = document.getElementById('voronoiBlurPass') as HTMLSelectElement;
  gridBlurPassElement: HTMLSelectElement = document.getElementById('gridBlurPass') as HTMLSelectElement;
  prevButtonElement: HTMLButtonElement = document.getElementById('prevButton') as HTMLButtonElement;
  nextButtonElement: HTMLButtonElement = document.getElementById('nextButton') as HTMLButtonElement;
  downloadMapButtonElement: HTMLButtonElement = document.getElementById('downloadMapButton') as HTMLButtonElement;
  settings: Settings = {
    mapName: 'Untitled Map',
    missionName: 'Untitled Mission',
    mapType: MapType.SingleIsland,
    biome: Biome.Boreal,
    edgeMode: EdgeMode.Water,
    coastSide: CoastSide.North,
    chunks: 8,
    worldWidth: (8 * 20) + 1,
    seed: Math.floor(Math.random() * 900000) + 100000,
    voronoiBlurPass: 5,
    gridBlurPass: 5,
    step: 0
  };
  downloadMapFunc: () => void = () => {};
  updateFunc: () => void = () => {};
  stepFunc: () => void = () => {};
  constructor() {
    this.mapNameElement.value = this.settings.mapName;
    this.mapNameElement.addEventListener('change', this.onchange.bind(this));
    this.missionNameElement.value = this.settings.missionName;
    this.missionNameElement.addEventListener('change', this.onchange.bind(this));
    this.seedElement.value = this.settings.seed.toString();
    this.seedElement.addEventListener('change', this.onchange.bind(this));
    this.newSeedButtonElement.addEventListener('click', this.newSeed.bind(this));
    this.chunksElement.value = this.settings.chunks.toString();
    this.chunksElement.addEventListener('change', this.onchange.bind(this));
    this.mapTypeElement.value = this.settings.mapType;
    this.mapTypeElement.addEventListener('change', this.onchange.bind(this));
    this.biomeElement.value = this.settings.biome;
    this.biomeElement.addEventListener('change', this.onchange.bind(this));
    this.voronoiBlurPassElement.value = this.settings.voronoiBlurPass.toString();
    this.voronoiBlurPassElement.addEventListener('change', this.onchange.bind(this));
    this.gridBlurPassElement.value = this.settings.gridBlurPass.toString();
    this.gridBlurPassElement.addEventListener('change', this.onchange.bind(this));
    this.prevButtonElement.addEventListener('click', this.prev.bind(this));
    this.nextButtonElement.addEventListener('click', this.next.bind(this));
    this.downloadMapButtonElement.addEventListener('click', this.downloadMap.bind(this));
  }
  prev() {
    this.settings.step = this.settings.step - 1;
    if(this.settings.step < 0) {
      this.settings.step = 0;
    }
    this.prevButtonElement.disabled = this.settings.step === 0;
    this.stepFunc();
  }
  next() {
    this.settings.step = this.settings.step + 1;
    if(this.settings.step < 0) {
      this.settings.step = 0;
    }
    this.prevButtonElement.disabled = this.settings.step === 0;
    this.stepFunc();
  }
  downloadMap() {
    this.downloadMapFunc();
  }
  newSeed() {
    this.settings.seed = Math.floor(Math.random() * 900000) + 100000;
    this.seedElement.value = this.settings.seed.toString();
    this.onchange();
  }
  onchange() {
    this.settings.mapName = this.mapNameElement.value;
    this.settings.missionName = this.missionNameElement.value;
    this.settings.seed = Number(this.seedElement.value);
    this.settings.chunks = Number(this.chunksElement.value);
    this.settings.worldWidth = (this.settings.chunks * 20) + 1;
    this.settings.mapType = this.mapTypeElement.value as MapType;
    this.settings.biome = this.biomeElement.value as Biome;
    this.settings.voronoiBlurPass = Number(this.voronoiBlurPassElement.value);
    this.settings.gridBlurPass = Number(this.gridBlurPassElement.value);
    this.updateFunc();
  }
}