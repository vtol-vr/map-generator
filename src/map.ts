import { PNG } from 'pngjs/browser';
import * as jpeg from 'jpeg-js';
import download from 'downloadjs';
import { BlobReader, BlobWriter, TextReader, ZipWriter } from '@zip.js/zip.js';
import { HeightMap, Settings } from './types';
import { colorFromHeight } from './renderUtilities';
import MapTemplate from './template';
import { nameToId } from './utilities';

export default class MapBuilder {
  heightMap: HeightMap = [];
  settings: Settings;
  mapTemplate: MapTemplate = new MapTemplate();
  searchMinMax(): number[] {
    let min = Number.MAX_VALUE;
    let max = Number.MIN_VALUE;
    for(let x = 0; x < this.settings.worldWidth;x++) {
      for(let y = 0; y < this.settings.worldWidth;y++) {
        if(this.heightMap[x][y] < min) {
          min = this.heightMap[x][y];
        }
        if(this.heightMap[x][y] > max) {
          max = this.heightMap[x][y];
        }
      }
    }
    return [min, max];
  }
  generateHeightMapPng(minElevation: number, maxElevation: number): Buffer {
    let png = new PNG({
      width: this.settings.worldWidth,
      height: this.settings.worldWidth,
      inputHasAlpha: true
    });
    for(let y = 0; y < png.height;y++) {
      for(let x = 0; x < png.width;x++) {
        let idx = (png.width * y + x) << 2;
        let elevation = this.heightMap[x][y];
        let elevationR = (elevation - minElevation) * (256 / (maxElevation - minElevation));
        elevationR = Math.min(255,Math.max(0,elevationR));
        png.data[idx] = Math.floor(elevationR);
        png.data[idx+1] = 0;
        png.data[idx+2] = 0;
        png.data[idx+3] = 255;
      }
    }
    return PNG.sync.write(png);
  }
  generatePreviewJpg(): Buffer {
    let jpg = {
      width: 512,
      height: 512,
      data: new Buffer(512 * 512 * 4)
    };
    for(let y = 0; y < jpg.height;y++) {
      for(let x = 0; x < jpg.width;x++) {
        let idx = (jpg.width * y + x) << 2;
        let elevation = this.heightMap[Math.floor((this.settings.worldWidth - 1) * (x / jpg.width))][Math.floor((this.settings.worldWidth - 1) * (y / jpg.height))];
        let color = colorFromHeight(elevation, this.settings.biome);
        jpg.data[idx] = (color & 0xff0000) >> 16;
        jpg.data[idx+1] = (color & 0x00ff00) >> 8;
        jpg.data[idx+2] = (color & 0x0000ff);
        jpg.data[idx+3] = 255;
      }
    }
    return jpeg.encode(jpg, 100).data;
  }
  async download() {
    let zipFile = new BlobWriter();
    let zipWriter = new ZipWriter(zipFile);
    await zipWriter.add(`${nameToId(this.settings.mapName)}/height0.png`, new BlobReader(new Blob([this.generateHeightMapPng(-80,1440)])));
    await zipWriter.add(`${nameToId(this.settings.mapName)}/height1.png`, new BlobReader(new Blob([this.generateHeightMapPng(1440,2960)])));
    await zipWriter.add(`${nameToId(this.settings.mapName)}/height2.png`, new BlobReader(new Blob([this.generateHeightMapPng(2960,4480)])));
    await zipWriter.add(`${nameToId(this.settings.mapName)}/height3.png`, new BlobReader(new Blob([this.generateHeightMapPng(4480,6000)])));
    await zipWriter.add(`${nameToId(this.settings.mapName)}/preview.jpg`, new BlobReader(new Blob([this.generatePreviewJpg()])));
    await zipWriter.add(`${nameToId(this.settings.mapName)}/${nameToId(this.settings.mapName)}.vtm`, new TextReader(this.mapTemplate.generateVTM(this.settings, { staticPrefabs: [], bezierRoads: [] })));
    await zipWriter.close();
    download(await zipFile.getData(), nameToId(this.settings.mapName) + '.zip', 'application/zip');
  }
}