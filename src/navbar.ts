import { Tab } from 'bootstrap';

export default class Navbar {
  showMap2DButtonElement: HTMLButtonElement = document.getElementById('showMap2DButton') as HTMLButtonElement;
  tabMap2D: Tab;
  map2DElement: HTMLDivElement = document.getElementById('map2D') as HTMLDivElement;
  showMap3DButtonElement: HTMLButtonElement = document.getElementById('showMap3DButton') as HTMLButtonElement;
  tabMap3D: Tab;
  map3DElement: HTMLDivElement = document.getElementById('map3D') as HTMLDivElement;
  constructor() {
    this.tabMap2D = new Tab(this.showMap2DButtonElement);
    this.tabMap3D = new Tab(this.showMap3DButtonElement);
    this.showMap2DButtonElement.addEventListener('click', this.onMap2D.bind(this));
    this.showMap3DButtonElement.addEventListener('click', this.onMap3D.bind(this));
  }
  onMap2D() {
    this.tabMap2D.show();
    this.map2DElement.style.display = 'block';
    this.map3DElement.style.display = 'none';
    window.dispatchEvent(new Event('resize'));
  }
  onMap3D() {
    this.tabMap3D.show();
    this.map2DElement.style.display = 'none';
    this.map3DElement.style.display = 'block';
    window.dispatchEvent(new Event('resize'));
  }
}