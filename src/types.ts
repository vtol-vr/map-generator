export enum MapType {
  SingleIsland = 'SingleIsland'
}

export enum Biome {
  Boreal = 'Boreal',
  Desert = 'Desert',
  Arctic = 'Arctic',
  Tropical = 'Tropical'
}

export enum EdgeMode {
  Water = 'Water',
  Hills = 'Hills',
  Coast = 'Coast'
}

export enum CoastSide {
  North = 'North',
  East = 'East',
  South = 'South',
  West = 'West'
}

export interface Settings {
  mapName: string;
  missionName: string;
  chunks: number;
  worldWidth: number;
  mapType: MapType;
  biome: Biome;
  edgeMode: EdgeMode;
  coastSide: string;
  seed: number;
  voronoiBlurPass: number;
  gridBlurPass: number;
  step: number;
}

export interface VoronoiCell {
  id: number;
  center: number[];
  points: number[][];
  neighbors: number[];
  initialHeight: number;
  blurHeights: number[];
  finalBlurHeight: number;
}

export interface MapTypeGeneratorOutput {
  voronoiCells: VoronoiCell[];
  edgeMode: EdgeMode;
  coastSide: CoastSide;
}

export type HeightMap = number[][];

export type Point = number[];

export interface CityCenter {
  x: number;
  y: number;
  radius: number;
}

export interface CityDistrict {
  id: number;
  center: number[];
  points: number[][];
  neighbors: number[];
  density: number;
  grid: RotatedGrid;
}

export interface RotatedGrid {
  gridSize: {
    x: number;
    y: number;
  };
  direction: number; //Radians
  //blocks: number[][][];
  gridlines: number[][][];
}

export interface VTMEntities {
  staticPrefabs: StaticPrefab[];
  bezierRoads: RoadChunk[];
}

export interface StaticPrefab {
  prefab: string;
  id: number;
  position: Position;
  rotation: Rotation
}

export interface Position {
  x: number;
  y: number;
  z: number;
}

export interface Rotation {
  x: number;
  y: number;
  z: number;
}

export interface ExtraFile {
  path: string;
  data: string;
}

export interface RoadChunk {
  grid: {
    x: number;
    y: number;
  };
  segments: RoadSegment[];
}

export interface RoadSegment {
  id: number;
  type: number;
  bridge: boolean;
  length: number;
  start: Position;
  middle: Position;
  end: Position;
  previousSegment: number | null;
  nextSegment: number | null;
}