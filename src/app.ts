import { Application, Graphics, Sprite, Texture } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import Menu from './menu';
import Generator from './generator';
import Navbar from './navbar';

export default class App {
  app: Application = new Application({ width: 640, height: 480 });
  navbar: Navbar = new Navbar();
  menu: Menu = new Menu();
  generator: Generator = new Generator();
  viewport: Viewport;
  graphics: Graphics = new Graphics();
  background: Sprite;
  constructor() {
    const map2DElement = document.getElementById('map2D') as HTMLDivElement;
    map2DElement.appendChild(this.app.view as any);
    this.app.resizeTo = map2DElement;

    this.viewport = new Viewport({
      screenWidth: map2DElement.offsetWidth,
      screenHeight: map2DElement.offsetHeight,
      worldWidth: this.menu.settings.worldWidth,
      worldHeight: this.menu.settings.worldWidth,
      events: this.app.renderer.events
    });
    this.viewport.fit();
    this.viewport.moveCenter(this.menu.settings.worldWidth / 2, this.menu.settings.worldWidth / 2);
    this.viewport.drag();
    this.viewport.pinch();
    this.viewport.wheel();
    this.viewport.decelerate();
    this.app.stage.addChild(this.viewport);

    this.background = this.viewport.addChild(new Sprite(Texture.WHITE));
    this.background.width = this.menu.settings.worldWidth;
    this.background.height = this.menu.settings.worldWidth;

    this.viewport.addChild(this.graphics);

    this.menu.updateFunc = this.menuSync.bind(this);
    this.menuSync();

    this.menu.stepFunc = () => {
      this.generator.stepRender(this.menu.settings, this.graphics);
    }

    this.menu.downloadMapFunc = () => {
      this.generator.downloadMap();
    }
  }
  menuSync() {
    const worldWidth = this.menu.settings.worldWidth;
    this.viewport.moveCenter(worldWidth / 2, worldWidth / 2);
    this.viewport.worldWidth = worldWidth;
    this.viewport.worldHeight = worldWidth;
    this.background.width = worldWidth;
    this.background.height = worldWidth;

    this.generator.generate(this.menu.settings);
    this.generator.stepRender(this.menu.settings, this.graphics);
  }
}