import { HeightMap } from './types';
import { World, Body, Heightfield, Vec3, RaycastResult, Ray } from 'cannon';

export default class TerrainSampler {
  heightMap: HeightMap = [];
  world = new World();
  heightField: Body;
  init() {
    this.heightField = new Body();
    this.heightField.addShape(new Heightfield(this.heightMap, {
      elementSize: 1
    }));
  }
  heightAt(x: number, y: number): number {
    let ray: Ray = new Ray(new Vec3(x, y, 10000), new Vec3(x, y, -10000));
    let result: RaycastResult = new RaycastResult();
    ray.intersectBody(this.heightField, result);
    return result.hitPointWorld.z;
  }
}