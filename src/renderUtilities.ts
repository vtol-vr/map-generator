import { Biome } from './types';

export function colorFromHeight(height: number, biome: Biome): number {
  if(biome === Biome.Boreal) {
    //Deep Sea
    if(height < -10) {
      return 0x0d156b;
    }
    //Shallow Water
    if(height < 0) {
      return 0x5eadf2;
    }
    //Sand
    if(height < 20) {
      return 0xd1c08e;
    }
    //Sand -> Grass
    if(height < 40) {
      return 0xc4d18e;
    }
    //Grass
    if(height < 1000) {
      return 0x207a1f;
    }
    //Grass -> Rocky
    if(height < 1250) {
      return 0x688268;
    }
    //Rocky
    if(height < 2500) {
      return 0x8a8a8a;
    }
    //Rocky -> Snow
    if(height < 4000) {
      return 0xc9c9c9;
    }
    //Snow
    return 0xededed;
  }
  return 0x000000;
}

export function colorFromRGB(r: number, g: number, b: number): number {
  return (r << 16) + (g << 8) + b;
}