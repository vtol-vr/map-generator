import { Graphics } from 'pixi.js';
import { CityCenter, CityDistrict, HeightMap, RotatedGrid, Settings } from './types';
import { MersenneTwister } from 'random-seedable';
import PoissonDiskSampling from 'poisson-disk-sampling';
import { Delaunay } from 'd3-delaunay';

const CITYCENTERPOISSONDISTANCE = 3;
const CITYSTARTSIZE = 5;
const CITYMAXSIZE = 15;
const CITYMINHEIGHT = 40;
const CITYMAXHEIGHT = 300;
const CITYMINDISTANCE = 15;
const CITYMINFLATNESS = 15;
const CITYDISTRICTPOISSONMINDISTANCE = 1;
const CITYDISTRICTPOISSONMAXDISTANCE = 4;
const CITYDISTRICTPOISSONTRIES = 3;
const CITYDISTRICTDENSITYRANDOM = 0.05;
const CITYDISTRICTMINRANDOMGRID = 0.125;
const CITYDISTRICTMAXRANDOMGRID = 0.25;

export default class CityBuilder {
  heightMap: HeightMap = [];
  settings: Settings;
  random: MersenneTwister;
  cityCenters: CityCenter[];
  cityDistrictDelaunay: Delaunay;
  cityDistrictVoronoi: Delaunay.Voronoi;
  cityDistricts: CityDistrict[];
  generate() {
    this.generateCityCenters();
    this.generateCityDistrict();
  }
  generateCityCenters() {
    console.info('Generating City Centers');
    this.cityCenters = [];
    //Generate poisson points for city center candidates
    const pds = new PoissonDiskSampling({
      shape: [this.settings.worldWidth, this.settings.worldWidth],
      minDistance: CITYCENTERPOISSONDISTANCE,
      maxDistance: CITYCENTERPOISSONDISTANCE,
      tries: 10
    }, () => { return this.random.float(); });
    const poissonPoints: number[][] = pds.fill();
    for(let poissonPoint of poissonPoints) {
      let size = CITYSTARTSIZE;
      let addCandidate = true;
      let continueChecking = true;
      let testCityCenter: CityCenter = { x: poissonPoint[0], y: poissonPoint[1], radius: size };
      while(continueChecking) {
        testCityCenter = { x: poissonPoint[0], y: poissonPoint[1], radius: size };
        let heights = this.heightsFromCityCenter(this.heightMap, testCityCenter);
        //Check that candidate is above min height
        if(!this.heightsInRange(heights)) {
          if(size === CITYSTARTSIZE) {
            addCandidate = false;
          }
          continueChecking = false;
        }
        //Check that candidate is not too close to existing city centers
        for(let tmpCityCenter of this.cityCenters) {
          const distance = Math.sqrt(((tmpCityCenter.x - testCityCenter.x) * (tmpCityCenter.x - testCityCenter.x)) + ((tmpCityCenter.y - testCityCenter.y) * (tmpCityCenter.y - testCityCenter.y)));
          if(distance < CITYMINDISTANCE + tmpCityCenter.radius + testCityCenter.radius) {
            if(size === CITYSTARTSIZE) {
              addCandidate = false;
            }
            continueChecking = false;
          }
        }
        //Check that candidate is flat enough
        let flatness = this.flatnessFromHeight(heights);
        if(flatness > CITYMINFLATNESS) {
          if(size === CITYSTARTSIZE) {
            addCandidate = false;
          }
          continueChecking = false;
        }
        //Stop checking if size is greater than max size
        continueChecking = continueChecking && size <= CITYMAXSIZE;
        size++;
      }
      if(addCandidate) {
        this.cityCenters.push(testCityCenter);
      }
    }
  }
  generateCityDistrict() {
    console.info('Generating City Districts');
    this.cityDistricts = [];
    //Generate poisson points for city districts
    const pds = new PoissonDiskSampling({
      shape: [this.settings.worldWidth, this.settings.worldWidth],
      minDistance: CITYDISTRICTPOISSONMINDISTANCE,
      maxDistance: CITYDISTRICTPOISSONMAXDISTANCE,
      tries: CITYDISTRICTPOISSONTRIES
    }, () => { return this.random.float(); });
    const poissonPoints: number[][] = pds.fill();
    this.cityDistrictDelaunay = Delaunay.from(poissonPoints);
    this.cityDistrictVoronoi = this.cityDistrictDelaunay.voronoi([0, 0, this.settings.worldWidth, this.settings.worldWidth]);
    for(let i = 0;i < poissonPoints.length;i++) {
      let inCityCenter = false;
      let closestCityCenter: CityCenter | null = null;
      let distanceCityCenter = -1;
      for(let cityCenter of this.cityCenters) {
        const distance = Math.sqrt(((cityCenter.x - poissonPoints[i][0]) * (cityCenter.x - poissonPoints[i][0])) + ((cityCenter.y - poissonPoints[i][1]) * (cityCenter.y - poissonPoints[i][1])));
        if(distance < cityCenter.radius) {
          //Point is within city center radius
          //Always add if distance is half the radius
          if(distance < cityCenter.radius / 2) {
            inCityCenter = true;
            closestCityCenter = cityCenter;
            distanceCityCenter = distance;
          }
          else {
            //Check if is neighbor with existing districts
            let isNeighbor = false;
            for(let cityDistrict of this.cityDistricts) {
              if(cityDistrict.neighbors.includes(i)) {
                isNeighbor = true;
              }
            }
            //Random chance to allow district if already neighbor
            if(isNeighbor) {
              if(this.random.float() > distance / cityCenter.radius) {
                inCityCenter = true;
                closestCityCenter = cityCenter;
                distanceCityCenter = distance;
              }
            }
          }
        }
      }
      if(inCityCenter && closestCityCenter !== null) {
        const neighbors: number[] = Array.from(this.cityDistrictVoronoi.neighbors(i));
        this.cityDistricts.push({
          id: i,
          center: poissonPoints[i],
          points: this.cityDistrictVoronoi.cellPolygon(i),
          neighbors: neighbors,
          density: (distanceCityCenter / closestCityCenter.radius) + (this.random.float() * CITYDISTRICTDENSITYRANDOM * 2) - CITYDISTRICTDENSITYRANDOM,
          grid: this.generateRotatedGrid(
            poissonPoints[i],
            this.cityDistrictVoronoi.cellPolygon(i),
            (this.random.float() * (CITYDISTRICTMAXRANDOMGRID - CITYDISTRICTMINRANDOMGRID)) + CITYDISTRICTMINRANDOMGRID,
            (this.random.float() * (CITYDISTRICTMAXRANDOMGRID - CITYDISTRICTMINRANDOMGRID)) + CITYDISTRICTMINRANDOMGRID,
            this.random.float() * 2 * Math.PI
          )
        });
      }
    }
  }
  heightsFromCityCenter(h: HeightMap, c: CityCenter): number[] {
    let res: number[] = [];
    for(let x = 0;x < h.length;x++) {
      for(let y = 0;y < h[x].length;y++) {
        let dist = Math.sqrt(((c.x - x) * (c.x - x)) + ((c.y - y) * (c.y - y)));
        if(dist <= c.radius) {
          res.push(h[x][y]);
        }
      }
    }
    return res;
  }
  heightsInRange(heights: number[]): boolean {
    let valid = true;
    for(let height of heights) {
      if(height < CITYMINHEIGHT || height > CITYMAXHEIGHT) {
        valid = false;
      }
    }
    return valid;
  }
  flatnessFromHeight(heights: number[]): number {
    //Returns average distance from the average height (close to zero the better)
    let sum = 0;
    for(let height of heights) {
      sum += height;
    }
    let avg = sum / heights.length;
    let flatnessSum = 0;
    for(let height of heights) {
      flatnessSum += Math.abs(height - avg);
    }
    return flatnessSum / heights.length;
  }
  generateRotatedGrid(center: number[], points: number[][], gridWidth: number, gridHeight: number, direction: number): RotatedGrid {
    //Find the furthest polygon point from center
    let maxDistance = 0;
    for(let point of points) {
      const distance = Math.sqrt(((center[0] - point[0]) * (center[0] - point[0])) + ((center[1] - point[1]) * (center[1] - point[1])));
      if(distance > maxDistance) {
        maxDistance = distance;
      }
    }
    //Add margin
    maxDistance += 1;
    //Start creating gridlines
    let gridlines: number[][][] = [];
    //Create positive X-axis gridlines
    for(let x = center[0];x < center[0] + maxDistance;x+=gridWidth) {
      let gridline: number[][] | null = [
        this.rotatePoint(center, [x, center[1] + maxDistance], direction),
        this.rotatePoint(center, [x, center[1] - maxDistance], direction)
      ];
      gridline = this.cutLine(points, gridline);
      if(gridline !== null) {
        gridlines.push(gridline);
      }
    }
    //Create negative X-axis gridlines
    for(let x = center[0];x > center[0] - maxDistance;x-=gridWidth) {
      let gridline: number[][] | null = [
        this.rotatePoint(center, [x, center[1] + maxDistance], direction),
        this.rotatePoint(center, [x, center[1] - maxDistance], direction)
      ];
      gridline = this.cutLine(points, gridline);
      if(gridline !== null) {
        gridlines.push(gridline);
      }
    }
    //Create positive Y-axis gridlines
    for(let y = center[0];y < center[1] + maxDistance;y+=gridHeight) {
      let gridline: number[][] | null = [
        this.rotatePoint(center, [center[0] + maxDistance, y], direction),
        this.rotatePoint(center, [center[0] - maxDistance, y], direction)
      ];
      gridline = this.cutLine(points, gridline);
      if(gridline !== null) {
        gridlines.push(gridline);
      }
    }
    //Create negative Y-axis gridlines
    for(let y = center[0];y > center[1] - maxDistance;y-=gridHeight) {
      let gridline: number[][] | null = [
        this.rotatePoint(center, [center[0] + maxDistance, y], direction),
        this.rotatePoint(center, [center[0] - maxDistance, y], direction)
      ];
      gridline = this.cutLine(points, gridline);
      if(gridline !== null) {
        gridlines.push(gridline);
      }
    }
    return {
      gridSize: {
        x: gridWidth,
        y: gridHeight
      },
      direction: direction,
      gridlines: gridlines
    };
  }
  rotatePoint(center: number[], point: number[], angle: number): number[] {
    return [
      Math.cos(angle) * (point[0] - center[0]) - Math.sin(angle) * (point[1] - center[1]) + center[0],
      Math.sin(angle) * (point[0] - center[0]) + Math.cos(angle) * (point[1] - center[1]) + center[1]
    ];
  }
  cutLine(points: number[][], line: number[][]): number[][] | null {
    let newPoints: number[][] = [];
    for(let i = 0;i < points.length;i++) {
      let prevI = i - 1;
      if(prevI < 0) {
        prevI = points.length - 1;
      }
      let polyLine = [ points[prevI], points[i] ];
      let intersect = this.lineIntersect(polyLine, line);
      if(intersect !== null) {
        newPoints.push(intersect);
      }
    }
    if(newPoints.length >= 2) {
      return [ newPoints[0], newPoints[1] ];
    }
    //If line doesn't intersect in two places, line is not in polygon
    return null;
  }
  lineIntersect(line1: number[][], line2: number[][]): number[] | null {
    let [ x1, y1 ] = line1[0];
    let [ x2, y2 ] = line1[1];
    let [ x3, y3 ] = line2[0];
    let [ x4, y4 ] = line2[1];
    //Check if none of the lines are of length 0
    if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
      return null;
    }
    let denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
    //Lines are parallel
    if (denominator === 0) {
      return null;
    }
    let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
    let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;
    //Is the intersection along the segments
    if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
      return null;
    }
    //Return a object with the x and y coordinates of the intersection
    let x = x1 + ua * (x2 - x1);
    let y = y1 + ua * (y2 - y1);
    return [x,y];
  }
  renderCityCenters(graphics: Graphics) {
    console.info('Rendering City Centers');
    for(let cityCenter of this.cityCenters) {
      graphics.beginFill(0xffffff, 0);
      graphics.lineStyle({
        width: 0.25,
        color: 0xff0000
      });
      graphics.drawCircle(cityCenter.x, cityCenter.y, cityCenter.radius);
      graphics.endFill();
    }
  }
  renderCityDistricts(graphics: Graphics) {
    console.info('Rendering City Districts');
    for(let cityDistrict of this.cityDistricts) {
      graphics.beginFill(0xffffff, 0);
      graphics.lineStyle({
        width: 0.125,
        color: 0xff0000
      });
      graphics.drawPolygon(cityDistrict.points.flat());
      graphics.lineStyle({
        width: 0.0625,
        color: 0xff0000
      });
      for(let gridline of cityDistrict.grid.gridlines) {
        graphics.moveTo(gridline[0][0], gridline[0][1]);
        graphics.lineTo(gridline[1][0], gridline[1][1]);
      }
      graphics.endFill();
    }
  }
}