import { MersenneTwister } from 'random-seedable';
import PoissonDiskSampling from 'poisson-disk-sampling';
import { Delaunay } from 'd3-delaunay';
import { HeightMap, MapType, Settings, VoronoiCell } from './types';
import { Graphics } from 'pixi.js';
import { colorFromHeight } from './renderUtilities';
import { generateSingleIsland } from './mapTypeGenerators/island';
import Chart from './chart';
import MapBuilder from './map';
import CityBuilder from './city';

export default class Generator {
  settings: Settings;
  chart: Chart = new Chart();
  mapBuilder: MapBuilder = new MapBuilder();
  cityBuilder: CityBuilder = new CityBuilder();
  worldWidth: number;
  random: MersenneTwister;
  poissonPoints: number[][];
  delaunay: Delaunay;
  voronoi: Delaunay.Voronoi;
  voronoiCells: VoronoiCell[];
  heightMap: HeightMap;
  generate(settings: Settings) {
    console.info('Starting Generation...');
    this.settings = settings;
    this.worldWidth = (this.settings.chunks * 20) + 1;
    this.random = new MersenneTwister(this.settings.seed, 624, 397);
    this.generatePoissonPoints();
    this.generateVoronoiCells();
    this.generateFromMapType();
    this.generateVoronoiBlur();
    this.generateHeightMap();
    this.generateGridBlur();
    this.generateRemap();
    this.chart.data(this.heightMap);
    this.generateCities();
  }
  generatePoissonPoints() {
    console.info('Generating Poisson Points');
    const pds = new PoissonDiskSampling({
      shape: [this.worldWidth, this.worldWidth],
      minDistance: 4,
      maxDistance: 4,
      tries: 10
    }, () => { return this.random.float(); });
    this.poissonPoints = pds.fill();
  }
  generateVoronoiCells() {
    console.info('Generating Voronoi Cells');
    this.delaunay = Delaunay.from(this.poissonPoints);
    this.voronoi = this.delaunay.voronoi([0, 0, this.worldWidth, this.worldWidth]);
    this.voronoiCells = [];
    for(let i = 0;i < this.poissonPoints.length;i++) {
      const neighbors: number[] = Array.from(this.voronoi.neighbors(i));
      this.voronoiCells.push({
        id: i,
        center: this.poissonPoints[i],
        points: this.voronoi.cellPolygon(i),
        neighbors: neighbors,
        initialHeight: 0,
        blurHeights: [],
        finalBlurHeight: 0
      });
    }
  }
  generateFromMapType() {
    console.info(`Generating Initial Heights using ${this.settings.mapType} Map Type`);
    if(this.settings.mapType === MapType.SingleIsland) {
      let output = generateSingleIsland(this.settings, this.random, this.voronoiCells);
      this.voronoiCells = output.voronoiCells;
      this.settings.edgeMode = output.edgeMode;
      this.settings.coastSide = output.coastSide;
    }
  }
  generateVoronoiBlur() {
    //Gaussian style blur (blur neighbors)
    console.info('Blurring Voronoi Cells');
    for(let i = 0;i < this.settings.voronoiBlurPass;i++) {
      for(let voronoiCell of this.voronoiCells) {
        let newValue = voronoiCell.initialHeight;
        if(i > 0) {
          newValue = voronoiCell.blurHeights[i - 1];
        }
        let total = newValue;
        //Sum up neighbors and self
        for(let neighbor of voronoiCell.neighbors) {
          let neighborCell = this.voronoiCells[neighbor];
          if(i === 0) {
            total += neighborCell.initialHeight;
          }
          else {
            total += neighborCell.blurHeights[i - 1];
          }
        }
        //New average
        newValue = total / (voronoiCell.neighbors.length + 1);
        voronoiCell.blurHeights.push(newValue);
      }
    }
    //Set final blur height
    for(let voronoiCell of this.voronoiCells) {
      voronoiCell.finalBlurHeight = voronoiCell.blurHeights[voronoiCell.blurHeights.length - 1];
    }
  }
  generateHeightMap() {
    console.info('Generating Height Map');
    this.heightMap = [];
    for(let x = 0;x < this.settings.worldWidth;x++) {
      this.heightMap[x] = [];
      for(let y = 0;y < this.settings.worldWidth;y++) {
        const cellIndex = this.delaunay.find(x, y);
        this.heightMap[x][y] = this.voronoiCells[cellIndex].finalBlurHeight;
      }
    }
  }
  generateGridBlur() {
    console.info('Blurring Height Map');
    for(let i = 0;i < this.settings.gridBlurPass;i++) {
      let newHeightMap: HeightMap = [];
      for(let x = 0;x < this.settings.worldWidth;x++) {
        newHeightMap[x] = [];
        for(let y = 0;y < this.settings.worldWidth;y++) {
          if(x > 0 && x < this.settings.worldWidth - 1 && y > 0 && y < this.settings.worldWidth - 1) {
            let neighborSum = 0;
            neighborSum += this.heightMap[x - 1][y - 1];
            neighborSum += this.heightMap[x - 0][y - 1];
            neighborSum += this.heightMap[x + 1][y - 1];
            neighborSum += this.heightMap[x - 1][y - 0];
            neighborSum += this.heightMap[x - 0][y - 0];
            neighborSum += this.heightMap[x + 1][y - 0];
            neighborSum += this.heightMap[x - 1][y + 1];
            neighborSum += this.heightMap[x - 0][y + 1];
            neighborSum += this.heightMap[x + 1][y + 1];
            newHeightMap[x][y] = neighborSum / 9;
          }
          else {
            newHeightMap[x][y] = this.heightMap[x][y];
          }
        }
      }
      this.heightMap = newHeightMap;
    }
  }
  generateRemap() {
    console.info('Remapping Height Map to Curve');
    for(let x = 0;x < this.settings.worldWidth;x++) {
      for(let y = 0;y < this.settings.worldWidth;y++) {
        if(this.heightMap[x][y] > 40) {
          this.heightMap[x][y] = 40 + (1 / (6000 * 6000)) * (this.heightMap[x][y] * this.heightMap[x][y] * this.heightMap[x][y]);
          //this.heightMap[x][y] = (1 / 6000) * (this.heightMap[x][y] * this.heightMap[x][y]);
        }
      }
    }
  }
  generateCities() {
    console.info('Generating Cities using CityBuilder');
    this.cityBuilder.heightMap = this.heightMap;
    this.cityBuilder.settings = this.settings;
    this.cityBuilder.random = this.random;
    this.cityBuilder.generate();
  }
  stepRender(settings: Settings, graphics: Graphics) {
    this.settings = settings;
    graphics.clear();
    if(this.settings.step === 1) {
      this.renderPoissonPoints(graphics);
    }
    if(this.settings.step === 2) {
      this.renderVoronoi(graphics, false);
    }
    if(this.settings.step === 3) {
      this.renderVoronoi(graphics, true, 'initialHeight');
    }
    if(this.settings.step === 4) {
      this.renderVoronoi(graphics, true, 'finalBlurHeight');
    }
    if(this.settings.step === 5) {
      this.renderHeightMap(graphics);
    }
    if(this.settings.step === 6) {
      this.renderOpacityMap(graphics);
    }
    if(this.settings.step === 7) {
      this.renderHeightMap(graphics);
      this.cityBuilder.renderCityCenters(graphics);
    }
    if(this.settings.step === 8) {
      this.renderHeightMap(graphics);
      this.cityBuilder.renderCityDistricts(graphics);
    }
  }
  renderPoissonPoints(graphics: Graphics) {
    console.info('Rendering Poisson Points');
    graphics.beginFill(0x000000);
    for(let poissonPoint of this.poissonPoints) {
      graphics.drawCircle(poissonPoint[0], poissonPoint[1], 0.5);
    }
  }
  renderVoronoi(graphics: Graphics, color: boolean, heightField: string = '') {
    if(color) {
      console.info(`Rendering Voronoi Cells with color (with '${heightField}')`);
    }
    else {
      console.info(`Rendering Voronoi Cells without color`);
    }
    for(let voronoiCell of this.voronoiCells) {
      if(color) {
        graphics.beginFill(colorFromHeight(voronoiCell[heightField], this.settings.biome));
      }
      else {
        graphics.beginFill(0xffffff);
      }
      graphics.lineStyle({
        width: 0.25,
        color: 0x000000
      });
      graphics.drawPolygon(voronoiCell.points.flat());
      graphics.endFill();
    }
  }
  renderHeightMap(graphics: Graphics) {
    console.info(`Rendering Height Map`);
    for(let x = 0;x < this.settings.worldWidth;x++) {
      for(let y = 0;y < this.settings.worldWidth;y++) {
        graphics.beginFill(colorFromHeight(this.heightMap[x][y], this.settings.biome));
        graphics.drawRect(x, y, 1, 1);
      }
    }
  }
  renderOpacityMap(graphics: Graphics) {
    console.info(`Rendering Opacity Map`);
    for(let x = 0;x < this.settings.worldWidth;x++) {
      for(let y = 0;y < this.settings.worldWidth;y++) {
        const percentage =  (this.heightMap[x][y] + 80) / (6000 + 80);
        const color = Math.floor(255 * percentage) << 16;
        graphics.beginFill(color);
        graphics.drawRect(x, y, 1, 1);
      }
    }
  }
  downloadMap() {
    this.mapBuilder.heightMap = this.heightMap;
    this.mapBuilder.settings = this.settings;
    this.mapBuilder.download();
  }
}