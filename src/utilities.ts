import { CoastSide, EdgeMode } from './types';

export function nameToId(name: string): string {
  return name.replace(/ /g,'_').toLowerCase();
}

export function chunksToMeter(chunks: number): number {
  return chunks * 3.072 * 1000;
}

export function mapX(x: number, chunks: number): number {
  return (x) * 153.6;
}

export function mapY(y: number, chunks: number): number {
  let mapWidth = chunks * 20 + 1;
  return ((mapWidth - y) - 1) * 153.6;
}

export function xInMap(x: number, chunks: number, edgeMode: EdgeMode, coastSide: CoastSide): boolean {
  let mapWidth = chunks * 20 + 1;
  let leftBoundary = mapWidth * 0.15;
  if(edgeMode === EdgeMode.Water || (edgeMode === EdgeMode.Coast && coastSide === CoastSide.West)) {
    leftBoundary = mapWidth * 0.1;
  }
  let rightBoundary = mapWidth * 0.15;
  if(edgeMode === EdgeMode.Water || (edgeMode === EdgeMode.Coast && coastSide === CoastSide.East)) {
    rightBoundary = mapWidth * 0.1;
  }
  return x >= leftBoundary && x <= (mapWidth - rightBoundary);
}

export function yInMap(y: number, chunks: number, edgeMode: EdgeMode, coastSide: CoastSide): boolean {
  let mapWidth = chunks * 20 + 1;
  let topBoundary = mapWidth * 0.15;
  if(edgeMode === EdgeMode.Water || (edgeMode === EdgeMode.Coast && coastSide === CoastSide.North)) {
    topBoundary = mapWidth * 0.1;
  }
  let bottomBoundary = mapWidth * 0.15;
  if(edgeMode === EdgeMode.Water || (edgeMode === EdgeMode.Coast && coastSide === CoastSide.South)) {
    bottomBoundary = mapWidth * 0.1;
  }
  return y >= topBoundary && y <= (mapWidth - bottomBoundary);
}